package logger

import (
	"testing"
)

func TestGetScoped(t *testing.T) {
	logger := GetScoped("testScope", nil)

	if logger == nil {
		t.Error("expected logger to be an instance of Logger")
	}

	if logger != nil && logger.scope != "testScope" {
		t.Errorf("expected logger to have the scope testScope got %s", logger.scope)
	}
}
