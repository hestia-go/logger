package logger

import (
	"gitlab.com/hestia-go/core/exception"
	"gitlab.com/hestia-go/core/is"
	"gitlab.com/hestia-go/logger/constant"
	"gitlab.com/hestia-go/logger/driver/console"
	_interface "gitlab.com/hestia-go/logger/interface"
)

// driverTypes List of registered drivers
var driverTypes = make(map[string]func(scope string, opts ..._interface.DriverOption) _interface.LoggerDriver)

// appName Store global app name
var appName = constant.LoggerDefaultAppName

// Logger Logger structure
type Logger struct {
	// drivers stores the logger drivers instances
	drivers []_interface.LoggerDriver
	// driversDeclarations stores the declarations of drivers to be instantiated
	driversDeclarations map[string][]_interface.DriverOption
	// allowedScopes stores the scopes allowed to be logged
	allowedScopes []string
	// scope store the logger scope
	scope string
}

// AppName Return app name
func AppName() string {
	return appName
}

// Debug Pass through message to logger debug method of its drivers
func (l *Logger) Debug(message string) {
	for _, driver := range l.drivers {
		driver.Debug(message)
	}
}

// ErrorString Pass through error string to logger error method of its drivers
func (l *Logger) ErrorString(err string) {
	for _, driver := range l.drivers {
		driver.ErrorString(err)
	}
}

// Error Pass through error object to logger error method of its drivers
func (l *Logger) Error(err error) {
	for _, driver := range l.drivers {
		driver.Error(err)
	}
}

// Exception Pass through exception to logger exception method of its drivers
func (l *Logger) Exception(exc *exception.Exception) {
	for _, driver := range l.drivers {
		driver.Exception(exc)
	}
}

// Info Pass through message to logger info method of its drivers
func (l *Logger) Info(message string) {
	for _, driver := range l.drivers {
		driver.Info(message)
	}
}

// Silly Pass through message to logger silly method of its drivers
func (l *Logger) Silly(message string) {
	for _, driver := range l.drivers {
		driver.Silly(message)
	}
}

// Verbose Pass through message to logger verbose method of its drivers
func (l *Logger) Verbose(message string) {
	for _, driver := range l.drivers {
		driver.Verbose(message)
	}
}

// Warn Pass through message to logger warn method of its drivers
func (l *Logger) Warn(message string) {
	for _, driver := range l.drivers {
		driver.Warn(message)
	}
}

// NewLogger Create a new logger for the given scope and options
func NewLogger(scope string, opts ...LoggerOption) *Logger {
	if is.Nil(opts) || (len(opts) == 1 && is.Nil(opts[0])) {
		opts = []LoggerOption{WithDrivers(map[string][]_interface.DriverOption{
			console.LoggerToken(): {console.WithAppName(AppName())},
		})}
	}

	logger := &Logger{
		allowedScopes: []string{constant.LoggerDefaultAllowedScopes},
		scope:         scope,
	}

	for _, opt := range opts {
		if opt != nil {
			opt(logger)
		}
	}

	createDrivers(logger, scope)

	return logger
}

// SetAppName Set global app name
func SetAppName(name string) {
	appName = name
}
