package logger

import (
	"testing"

	"gitlab.com/hestia-go/logger/driver/console"
	"gitlab.com/hestia-go/logger/driver/file"
)

func TestInit(t *testing.T) {
	LoggerInit()

	if driverTypes[console.LoggerToken()] == nil {
		t.Error("expected driverTypes to contains console logger token")
	}

	if driverTypes[file.LoggerToken()] == nil {
		t.Error("expected driverTypes to contains file logger token")
	}
}
