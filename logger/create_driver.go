package logger

import (
	"gitlab.com/hestia-go/core/is"
	"gitlab.com/hestia-go/logger/driver/console"
	_interface "gitlab.com/hestia-go/logger/interface"
)

// createDrivers Create drivers based on given options for given scope
func createDrivers(logger *Logger, scope string) {
	var drivers []_interface.LoggerDriver

	if is.Nil(logger.driversDeclarations) {
		logger.driversDeclarations = make(map[string][]_interface.DriverOption)
	}

	if len(logger.driversDeclarations) == 0 {
		logger.driversDeclarations[console.LoggerToken()] = []_interface.DriverOption{console.WithAppName(AppName())}
	}

	for name, opts := range logger.driversDeclarations {
		driver := createDriver(name, opts, scope)
		if !is.Nil(driver) {
			if driver.AppName() == "" {
				driver.SetAppName(AppName())
			}
			drivers = append(drivers, driver)
		}
	}

	logger.drivers = drivers
}

// createDriver Create a named driver based on options for the given scope
func createDriver(name string, opts []_interface.DriverOption, scope string) _interface.LoggerDriver {
	if _, ok := driverTypes[name]; !ok {
		return nil
	}

	if !is.Nil(driverTypes[name]) && is.Func(driverTypes[name]) {
		return driverTypes[name](scope, opts...)
	}

	return nil
}
