package logger

import (
	"gitlab.com/hestia-go/logger/driver/console"
	"gitlab.com/hestia-go/logger/driver/file"
)

// LoggerInit RegisterLoggerDriver internal logger drivers
func LoggerInit() {
	RegisterLoggerDriver(console.LoggerToken(), console.NewConsole)
	RegisterLoggerDriver(file.LoggerToken(), file.NewFile)
}
