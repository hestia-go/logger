package logger

import _interface "gitlab.com/hestia-go/logger/interface"

type LoggerOption func(*Logger)

// WithAllowedScopes set the allowed log scopes in the logger
func WithAllowedScopes(scopes ...string) LoggerOption {
	return func(l *Logger) {
		l.allowedScopes = scopes
	}
}

// WithDrivers set the drivers declaration in the logger
func WithDrivers(drivers map[string][]_interface.DriverOption) LoggerOption {
	return func (l *Logger) {
		l.driversDeclarations = drivers
	}
}
