package logger

import (
	"testing"

	"gitlab.com/hestia-go/core/slice"
	_interface "gitlab.com/hestia-go/logger/interface"
)

func TestWithAllowedScopes(t *testing.T) {
	logger := &Logger{}

	WithAllowedScopes("a", "b", "c")(logger)

	if !slice.Contains(logger.allowedScopes, "a") || !slice.Contains(logger.allowedScopes, "b") || !slice.Contains(logger.allowedScopes, "c") {
		t.Errorf("expected allowed scopes to have value a, b, c got %+v", logger.allowedScopes)
	}
}

func TestWithDrivers(t * testing.T) {
	logger := &Logger{}

	WithDrivers(map[string][]_interface.DriverOption{
		"test": {},
	})(logger)

	if logger.driversDeclarations["test"] == nil {
		t.Errorf("expected logger to contains the given driver declaration")
	}
}
