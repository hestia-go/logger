package logger

import (
	"testing"

	"gitlab.com/hestia-go/logger/driver/console"
)

func TestRegister(t *testing.T) {
	RegisterLoggerDriver(console.LoggerToken(), console.NewConsole)

	if driverTypes[console.LoggerToken()] == nil {
		t.Error("expected driverTypes console logger token")
	}

	if driverTypes[console.LoggerToken()] != nil {
		driver := driverTypes[console.LoggerToken()]("test")

		if _, ok := driver.(*console.Driver); !ok {
			t.Error("expected driverType console constructor to return a console driver")
		}
	}
}
