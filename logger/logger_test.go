package logger

import (
	"errors"
	"fmt"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/fatih/color"
	"github.com/hhkbp2/go-strftime"
	"gitlab.com/hestia-go/core/exception"
	"gitlab.com/hestia-go/core/registry"
	"gitlab.com/hestia-go/logger/constant"
	"gitlab.com/hestia-go/logger/driver/console"
	_interface "gitlab.com/hestia-go/logger/interface"
)

func TestLogger_Debug(t *testing.T) {
	registry.InitRegistry()
	LoggerInit()
	logger := GetDefault()

	consoleDriver := logger.drivers[0].(*console.Driver)

	outputStd, err := os.CreateTemp("", "outputStd")
	if err != nil {
		t.Fatalf("failed to create temporary file: %v", err)
	}
	defer os.Remove(outputStd.Name())

	consoleDriver.OutputStd = outputStd

	logger.Debug("test debug")

	// Check the output in outputStd file
	outputBytes, err := os.ReadFile(outputStd.Name())
	if err != nil {
		t.Fatalf("failed to read outputStd file: %v", err)
	}
	green := consoleDriver.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()
	white := consoleDriver.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	blue := consoleDriver.ApplyColorSetting(color.New(color.FgBlue)).SprintfFunc()
	expectedOutput := fmt.Sprintf(
		constant.LoggerStdFormat,
		green(constant.LoggerAppNameFormat, "App"),
		"Debug",
		white("%s", strftime.Format(constant.LoggerDateTimeFormat, time.Now())),
		"test debug",
	)
	expectedOutput = blue(expectedOutput) + "\n"
	if string(outputBytes) != expectedOutput {
		t.Errorf("expected output %q but got %q", expectedOutput, string(outputBytes))
	}
}

func TestLogger_ErrorString(t *testing.T) {
	registry.InitRegistry()
	LoggerInit()
	logger := GetDefault()

	consoleDriver := logger.drivers[0].(*console.Driver)

	outputErr, err := os.CreateTemp("", "outputErr")
	if err != nil {
		t.Fatalf("failed to create temporary file: %v", err)
	}
	defer os.Remove(outputErr.Name())

	consoleDriver.OutputErr = outputErr

	logger.ErrorString("test error string")

	outputBytes, err := os.ReadFile(outputErr.Name())
	if err != nil {
		t.Fatalf("failed to read outputErr file: %v", err)
	}

	green := consoleDriver.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()
	white := consoleDriver.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	red := consoleDriver.ApplyColorSetting(color.New(color.FgRed)).SprintfFunc()
	expectedOutput := fmt.Sprintf(
		constant.LoggerErrorFormat,
		green(constant.LoggerAppNameFormat, "App"),
		white("%s", strftime.Format(constant.LoggerDateTimeFormat, time.Now())),
		"test error string",
	)
	expectedOutput = red(expectedOutput) + "\n"
	if !strings.Contains(string(outputBytes), expectedOutput) {
		t.Errorf("expected string start %q but got %q", expectedOutput, string(outputBytes))
	}
}

func TestLogger_Error(t *testing.T) {
	registry.InitRegistry()
	LoggerInit()
	logger := GetDefault()

	consoleDriver := logger.drivers[0].(*console.Driver)

	outputErr, err := os.CreateTemp("", "outputErr")
	if err != nil {
		t.Fatalf("failed to create temporary file: %v", err)
	}
	defer os.Remove(outputErr.Name())

	consoleDriver.OutputErr = outputErr

	localError := errors.New("test error object")
	logger.Error(localError)

	outputBytes, err := os.ReadFile(outputErr.Name())
	if err != nil {
		t.Fatalf("failed to read outputErr file: %v", err)
	}

	green := consoleDriver.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()
	white := consoleDriver.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	red := consoleDriver.ApplyColorSetting(color.New(color.FgRed)).SprintfFunc()
	expectedOutput := fmt.Sprintf(
		constant.LoggerErrorFormat,
		green(constant.LoggerAppNameFormat, "App"),
		white("%s", strftime.Format(constant.LoggerDateTimeFormat, time.Now())),
		localError.Error(),
	)
	expectedOutput = red(expectedOutput)
	expectedOutput = expectedOutput[:71]
	if !strings.Contains(string(outputBytes), expectedOutput) {
		t.Errorf("expected string start %q but got %q", expectedOutput, string(outputBytes))
	}
}

func TestLogger_Exception(t *testing.T) {
	registry.InitRegistry()
	LoggerInit()
	logger := GetDefault()

	consoleDriver := logger.drivers[0].(*console.Driver)

	// Create a temporary file for outputErr
	outputErr, err := os.CreateTemp("", "outputErr")
	if err != nil {
		t.Fatalf("failed to create temporary file: %v", err)
	}
	defer os.Remove(outputErr.Name())

	consoleDriver.OutputErr = outputErr

	localException := exception.NewException(exception.WithCode("tst1"), exception.WithMessage("test exception"))
	logger.Exception(localException)

	// Check the output in outputErr file
	outputBytes, err := os.ReadFile(outputErr.Name())
	if err != nil {
		t.Fatalf("failed to read outputErr file: %v", err)
	}
	// expectedOutput := fmt.Sprintf("%s %s\n", color.RedString("[test-scope]"), "[test-code] test exception message")
	expectedOutput := fmt.Sprintf(
		constant.LoggerExceptionFormat,
		localException.Code(),
		"",
		localException.Message(),
		nil,
	)

	green := consoleDriver.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()
	white := consoleDriver.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	red := consoleDriver.ApplyColorSetting(color.New(color.FgRed)).SprintfFunc()
	expectedOutput = fmt.Sprintf(
		constant.LoggerErrorFormat,
		green(constant.LoggerAppNameFormat, "App"),
		white("%s", strftime.Format(constant.LoggerDateTimeFormat, time.Now())),
		expectedOutput,
	)
	expectedOutput = red(expectedOutput)
	if !strings.Contains(string(outputBytes), expectedOutput) {
		t.Errorf("expected output %q but got %q", expectedOutput, string(outputBytes))
	}
}

func TestLogger_Silly(t *testing.T) {
	registry.InitRegistry()
	LoggerInit()
	logger := GetDefault()

	consoleDriver := logger.drivers[0].(*console.Driver)

	// Create a temporary file for outputStd
	outputStd, err := os.CreateTemp("", "outputStd")
	if err != nil {
		t.Fatalf("failed to create temporary file: %v", err)
	}
	defer os.Remove(outputStd.Name())

	consoleDriver.OutputStd = outputStd

	logger.Silly("test silly log")

	// Check the output in outputStd file
	outputBytes, err := os.ReadFile(outputStd.Name())
	if err != nil {
		t.Fatalf("failed to read outputStd file: %v", err)
	}
	green := consoleDriver.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()
	white := consoleDriver.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	hiWhite := consoleDriver.ApplyColorSetting(color.New(color.FgHiWhite)).SprintfFunc()
	expectedOutput := fmt.Sprintf(
		constant.LoggerStdFormat,
		green(constant.LoggerAppNameFormat, "App"),
		"Silly",
		white("%s", strftime.Format(constant.LoggerDateTimeFormat, time.Now())),
		"test silly log",
	)
	expectedOutput = hiWhite(expectedOutput) + "\n"
	if string(outputBytes) != expectedOutput {
		t.Errorf("expected output %q but got %q", expectedOutput, string(outputBytes))
	}
}

func TestLogger_Verbose(t *testing.T) {
	registry.InitRegistry()
	LoggerInit()
	logger := GetDefault()

	consoleDriver := logger.drivers[0].(*console.Driver)

	// Create a temporary file for outputStd
	outputStd, err := os.CreateTemp("", "outputStd")
	if err != nil {
		t.Fatalf("failed to create temporary file: %v", err)
	}
	defer os.Remove(outputStd.Name())

	consoleDriver.OutputStd = outputStd

	logger.Verbose("test verbose log")

	// Check the output in outputStd file
	outputBytes, err := os.ReadFile(outputStd.Name())
	if err != nil {
		t.Fatalf("failed to read outputStd file: %v", err)
	}
	green := consoleDriver.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()
	white := consoleDriver.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	hiCyan := consoleDriver.ApplyColorSetting(color.New(color.FgHiCyan)).SprintfFunc()
	expectedOutput := fmt.Sprintf(
		constant.LoggerStdFormat,
		green(constant.LoggerAppNameFormat, "App"),
		"Verbose",
		white("%s", strftime.Format(constant.LoggerDateTimeFormat, time.Now())),
		"test verbose log",
	)
	expectedOutput = hiCyan(expectedOutput) + "\n"
	if string(outputBytes) != expectedOutput {
		t.Errorf("expected output %q but got %q", expectedOutput, string(outputBytes))
	}
}

func TestLogger_Warn(t *testing.T) {
	registry.InitRegistry()
	LoggerInit()
	logger := GetDefault()

	consoleDriver := logger.drivers[0].(*console.Driver)

	// Create a temporary file for outputStd
	outputStd, err := os.CreateTemp("", "outputStd")
	if err != nil {
		t.Fatalf("failed to create temporary file: %v", err)
	}
	defer os.Remove(outputStd.Name())

	consoleDriver.OutputStd = outputStd

	logger.Warn("test warn log")

	// Check the output in outputStd file
	outputBytes, err := os.ReadFile(outputStd.Name())
	if err != nil {
		t.Fatalf("failed to read outputStd file: %v", err)
	}
	green := consoleDriver.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()
	white := consoleDriver.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	yellow := consoleDriver.ApplyColorSetting(color.New(color.FgYellow)).SprintfFunc()
	expectedOutput := fmt.Sprintf(
		constant.LoggerStdFormat,
		green(constant.LoggerAppNameFormat, "App"),
		"Warn",
		white("%s", strftime.Format(constant.LoggerDateTimeFormat, time.Now())),
		"test warn log",
	)
	expectedOutput = yellow(expectedOutput) + "\n"
	if string(outputBytes) != expectedOutput {
		t.Errorf("expected output %q but got %q", expectedOutput, string(outputBytes))
	}
}

func TestLogger_Info(t *testing.T) {
	registry.InitRegistry()
	LoggerInit()
	logger := GetDefault()

	consoleDriver := logger.drivers[0].(*console.Driver)

	// Create a temporary file for outputStd
	outputStd, err := os.CreateTemp("", "outputStd")
	if err != nil {
		t.Fatalf("failed to create temporary file: %v", err)
	}
	defer os.Remove(outputStd.Name())

	consoleDriver.OutputStd = outputStd

	logger.Info("test info log")

	// Check the output in outputStd file
	outputBytes, err := os.ReadFile(outputStd.Name())
	if err != nil {
		t.Fatalf("failed to read outputStd file: %v", err)
	}
	green := consoleDriver.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()
	white := consoleDriver.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	cyan := consoleDriver.ApplyColorSetting(color.New(color.FgCyan)).SprintfFunc()
	expectedOutput := fmt.Sprintf(
		constant.LoggerStdFormat,
		green(constant.LoggerAppNameFormat, "App"),
		"Info",
		white("%s", strftime.Format(constant.LoggerDateTimeFormat, time.Now())),
		"test info log",
	)
	expectedOutput = cyan(expectedOutput) + "\n"
	if string(outputBytes) != expectedOutput {
		t.Errorf("expected output %q but got %q", expectedOutput, string(outputBytes))
	}
}

func TestNewLogger(t *testing.T) {
	logger := NewLogger("test", nil)

	if logger == nil {
		t.Errorf("expected logger instance to be an instance of Logger")
	}
}

func TestNewLoggerDefaultOptions(t *testing.T) {
	logger := NewLogger("test2", nil)

	if logger == nil {
		t.Errorf("expected logger instance to be an instance of Logger")
	}

	if logger != nil && len(logger.drivers) != 1 {
		t.Errorf("expected logger to have one driver, logger has %d drivers", len(logger.drivers))
	}

	if logger != nil && logger.drivers[0] != nil {
		if _, ok := logger.drivers[0].(*console.Driver); !ok {
			t.Errorf("expected default logger to have a console driver")
		}
	}

	if logger != nil && logger.drivers[0] != nil && logger.drivers[0].AppName() != constant.LoggerDefaultAppName {
		t.Errorf("expected to have the default app name got %s", logger.drivers[0].AppName())
	}
}

func TestNewLoggerWithAnUnknownDriver(t *testing.T) {
	logger := NewLogger("test", WithDrivers(map[string][]_interface.DriverOption{
		"test": nil,
	}))

	if logger == nil {
		t.Errorf("expected to have a logger instance")
	}

	if logger != nil && len(logger.drivers) != 0 {
		t.Errorf("expected the logger to not have a driver")
	}
}

func TestNewLoggerWithAInvalidDriverConstructor(t *testing.T) {
	RegisterLoggerDriver("test", nil)

	logger := NewLogger("test", WithDrivers(map[string][]_interface.DriverOption{
		"test": nil,
	}))

	if logger == nil {
		t.Errorf("expected to have a logger instance")
	}

	if logger != nil && len(logger.drivers) != 0 {
		t.Errorf("expected the logger to not have a driver")
	}
}

func TestAppName(t *testing.T) {
	appName = "testAppName"

	if AppName() != "testAppName" {
		t.Errorf("expected to have testAppName app name got %s", AppName())
	}

	appName = constant.LoggerDefaultAppName
}

func TestSetAppName(t *testing.T) {
	SetAppName("testAppName")

	if appName != "testAppName" {
		t.Errorf("expected appName to be equal to testAppName got %s", appName)
	}

	SetAppName(constant.LoggerDefaultAppName)
}
