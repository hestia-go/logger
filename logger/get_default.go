package logger

import (
	"gitlab.com/hestia-go/logger/constant"
	"gitlab.com/hestia-go/logger/driver/console"
	_interface "gitlab.com/hestia-go/logger/interface"
)

// GetDefault Create and / or return the default scoped logger instance
func GetDefault() *Logger {
	return GetScoped(constant.LoggerDefaultLoggerScope, WithDrivers(map[string][]_interface.DriverOption{
		console.LoggerToken(): {console.WithAppName(AppName())},
	}))
}
