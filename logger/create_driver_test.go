package logger

import (
	"testing"

	"gitlab.com/hestia-go/logger/driver/console"
	_interface "gitlab.com/hestia-go/logger/interface"
)

func TestCreateDriverWithoutLoggerDeclaration(t * testing.T) {
	LoggerInit()

	logger := &Logger{}
	createDrivers(logger, "test")

	if len(logger.drivers) == 0 {
		t.Error("Create drivers must setup a logger if no definitions are set")
	}

	if _, ok := logger.drivers[0].(*console.Driver); !ok {
		t.Error("Create drivers must setup a console logger")
	}
}

func TestCreateDriverWithLoggerDeclarationButWithoutAppName(t* testing.T) {
	LoggerInit()

	logger := &Logger{
		scope: "test",
		driversDeclarations: map[string][]_interface.DriverOption{
			console.LoggerToken(): {},
		},
	}

	createDrivers(logger, logger.scope)

	if len(logger.drivers) == 0 {
		t.Error("Create drivers must setup the logger defined")
	}

	if _, ok := logger.drivers[0].(*console.Driver); !ok {
		t.Error("Create drivers must setup the console driver as defined")
	}

	if c, ok := logger.drivers[0].(*console.Driver); ok && c.AppName() != appName {
		t.Error("Create drivers must affect the app name to the logger")
	}
}
