package logger

import _interface "gitlab.com/hestia-go/logger/interface"

// RegisterLoggerDriver RegisterLoggerDriver a logger driver
func RegisterLoggerDriver(name string, factory func(scope string, opts ..._interface.DriverOption) _interface.LoggerDriver) {
	driverTypes[name] = factory
}
