package logger

import (
	"gitlab.com/hestia-go/core/registry"
	"gitlab.com/hestia-go/core/registry/driver/memory"
	"gitlab.com/hestia-go/logger/constant"
)

// GetScoped Create and / or return the scoped driver
func GetScoped(scope string, opts ...LoggerOption) *Logger {
	reg := registry.GetInstance(memory.RegistryToken())

	key := constant.LoggerRegistryPrefix + scope

	if driver, ok := reg.Get(key).(*Logger); ok {
		return driver
	}

	driver := NewLogger(scope, opts...)
	reg.Set(key, driver)

	return driver
}
