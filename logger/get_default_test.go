package logger

import (
	"testing"

	"gitlab.com/hestia-go/core/registry"
	"gitlab.com/hestia-go/core/registry/driver/memory"
	"gitlab.com/hestia-go/logger/driver/console"
)

func TestGetDefault(t *testing.T) {
	registry.InitRegistry()
	LoggerInit()
	logger := GetDefault()

	if logger == nil {
		t.Errorf("expected to return a logger instance received nil")
	}

	if registry.GetInstance(memory.RegistryToken()).Has("logger-Default") == false {
		t.Errorf("expected to have the default logger in registry")
	}

	if logger != nil && logger.drivers[0] != nil {
		if _, ok := logger.drivers[0].(*console.Driver); !ok {
			t.Errorf("expected logger to have a console driver")
		}
	}
}
