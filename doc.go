package logger

// blank imports for docs

import (
	// constants
	_ "gitlab.com/hestia-go/logger/constant"
	// console driver
	_ "gitlab.com/hestia-go/logger/driver/console"
	// file driver
	_ "gitlab.com/hestia-go/logger/driver/file"
	// formatter
	_ "gitlab.com/hestia-go/logger/formatter"
	// interfaces
	_ "gitlab.com/hestia-go/logger/interface"
	// logger
	_ "gitlab.com/hestia-go/logger/logger"
)
