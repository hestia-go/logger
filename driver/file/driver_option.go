package file

import (
	"gitlab.com/hestia-go/core/exception"
	_interface "gitlab.com/hestia-go/logger/interface"
)

// WithAppName set the application name in the file driver
func WithAppName(appName string) _interface.DriverOption {
	return func(d _interface.LoggerDriver) error {
		if driver, ok := d.(*Driver); ok {
			driver.appName = appName
			return nil
		}

		return exception.NewException(exception.WithMessage("The driver must be an instance of File driver for option WithAppName"))
	}
}

// WithLogPath set the path where to create log files in the file driver
func WithLogPath(logPath string) _interface.DriverOption {
	return func(d _interface.LoggerDriver) error {
		if driver, ok := d.(*Driver); ok {
			driver.logPath = logPath
			return nil
		}

		return exception.NewException(exception.WithMessage("The driver must be an instance of File driver for option WithLogPath"))
	}
}

// WithSeparatedFiles set the file driver to separate log levels to their own log files
func WithSeparatedFiles() _interface.DriverOption {
	return func(d _interface.LoggerDriver) error {
		if driver, ok := d.(*Driver); ok {
			driver.separatedFiles = true
			return nil
		}

		return exception.NewException(exception.WithMessage("The driver must be an instance of File driver for option WithSeparatedFiles"))
	}
}
