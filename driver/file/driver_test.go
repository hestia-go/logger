package file

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/fatih/color"
	"github.com/hhkbp2/go-strftime"
	"gitlab.com/hestia-go/core/exception"
	"gitlab.com/hestia-go/core/runtime"
	"gitlab.com/hestia-go/logger/constant"
)

func TestDriver_AppName(t *testing.T) {
	// Initialize a new Driver struct for testing
	d := &Driver{
		appName: "myapp",
	}

	// Test the AppName method
	if d.AppName() != "myapp" {
		t.Errorf("expected AppName to return 'myapp', got %v", d.AppName())
	}
}

func TestDriver_Debug(t *testing.T) {
	// Initialize a new Driver struct for testing
	d := &Driver{
		logPath: "/var/log/myapp",
		appName: "test-app",
		scope:   "test-scope",
	}

	// Set up a mock file for testing
	file, err := os.CreateTemp("", "myapp_debug")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	// Set up the writer for the debug level
	d.writers = &sync.Map{}
	d.writers.Store(constant.LoggerCombinedFileName, file)

	// Call the Debug method
	if err := d.Debug("debug message"); err != nil {
		t.Fatalf("Debug method returned an error: %v", err)
	}

	// Check if the message was logged to the correct file
	fileContent, err := os.ReadFile(file.Name())
	if err != nil {
		t.Fatal(err)
	}

	expectedLog := fmt.Sprintf("[test-app] <Debug> %s: debug message\n", strftime.Format(constant.LoggerDateTimeFormat, time.Now()))
	if string(fileContent) != expectedLog {
		t.Errorf("expected log to contain '%s', got %s", expectedLog, string(fileContent))
	}
}

func TestDriver_ErrorString(t *testing.T) {
	// Initialize a new Driver struct for testing
	d := &Driver{
		logPath: "/var/log/myapp",
		appName: "test-app",
		scope:   "test-scope",
	}

	// Set up a mock file for testing
	file, err := os.CreateTemp("", "myapp_error")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	// Set up the writer for the error level
	d.writers = &sync.Map{}
	d.writers.Store(constant.LoggerCombinedFileName, file)

	// Call the ErrorString method
	if err := d.ErrorString("error message"); err != nil {
		t.Fatalf("ErrorString method returned an error: %v", err)
	}

	// Check if the message was logged to the correct file
	fileContent, err := os.ReadFile(file.Name())
	if err != nil {
		t.Fatal(err)
	}

	expectedLog := fmt.Sprintf("[test-app] %s Error: error message\n", strftime.Format(constant.LoggerDateTimeFormat, time.Now()))
	if string(fileContent) != expectedLog {
		t.Errorf("expected log to contain '%s', got %s", expectedLog, string(fileContent))
	}
}

func TestDriver_Error(t *testing.T) {
	// Initialize a new Driver struct for testing
	d := &Driver{
		logPath: "/var/log/myapp",
		appName: "test-app",
		scope:   "test-scope",
	}

	// Set up a mock file for testing
	file, err := os.CreateTemp("", "myapp_error")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	// Set up the writer for the error level
	d.writers = &sync.Map{}
	d.writers.Store(constant.LoggerCombinedFileName, file)

	// Call the Error method with a known error object
	errObj := errors.New("test error")
	if err := d.Error(errObj); err != nil {
		t.Fatalf("Error method returned an error: %v", err)
	}

	// Check if the message was logged to the correct file
	fileContent, err := os.ReadFile(file.Name())
	if err != nil {
		t.Fatal(err)
	}

	expectedLog := fmt.Sprintf("[test-app] %s Error: test error", strftime.Format(constant.LoggerDateTimeFormat, time.Now()))
	if !strings.Contains(string(fileContent), expectedLog) {
		t.Errorf("expected log to contain '%s', got %s", expectedLog, string(fileContent))
	}
}

func TestDriver_Exception(t *testing.T) {
	// Initialize a new Driver struct for testing
	d := &Driver{
		logPath: "/var/log/myapp",
		appName: "test-app",
		scope:   "test-scope",
	}

	// Set up a mock file for testing
	file, err := os.CreateTemp("", "myapp_error")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	// Set up the writer for the error level
	d.writers = &sync.Map{}
	d.writers.Store(constant.LoggerCombinedFileName, file)

	// Call the Exception method with a known exception object
	excObj := exception.NewException(exception.WithMessage("test exception"), exception.WithCode("code"))
	if err := d.Exception(excObj); err != nil {
		t.Fatalf("Exception method returned an error: %v", err)
	}

	// Check if the message was logged to the correct file
	fileContent, err := os.ReadFile(file.Name())
	if err != nil {
		t.Fatal(err)
	}

	expectedLog := fmt.Sprintf("[test-app] %s Error: [code] test exception\n", strftime.Format(constant.LoggerDateTimeFormat, time.Now()))
	if !strings.Contains(string(fileContent), expectedLog) {
		t.Errorf("expected log to contain '%s', got %s", expectedLog, string(fileContent))
	}
}

func TestDriver_Warn(t *testing.T) {
	// Initialize a new Driver struct for testing
	d := &Driver{
		logPath: "/var/log/myapp",
		appName: "test-app",
		scope:   "test-scope",
	}

	// Set up a mock file for testing
	file, err := os.CreateTemp("", "myapp_warn")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	// Set up the writer for the warn level
	d.writers = &sync.Map{}
	d.writers.Store(constant.LoggerCombinedFileName, file)

	// Call the Warn method with a message
	message := "test warn message"
	if err := d.Warn(message); err != nil {
		t.Fatalf("Warn method returned an error: %v", err)
	}

	// Check if the message was logged to the correct file
	fileContent, err := os.ReadFile(file.Name())
	if err != nil {
		t.Fatal(err)
	}

	expectedLog := fmt.Sprintf("[test-app] <Warn> %s: %s\n", strftime.Format(constant.LoggerDateTimeFormat, time.Now()), message)
	if string(fileContent) != expectedLog {
		t.Errorf("expected log to contain '%s', got %s", expectedLog, string(fileContent))
	}
}

func TestDriver_Info(t *testing.T) {
	// Initialize a new Driver struct for testing
	d := &Driver{
		logPath: "/var/log/myapp",
		appName: "test-app",
		scope:   "test-scope",
	}

	// Set up a mock file for testing
	file, err := os.CreateTemp("", "myapp_info")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	// Set up the writer for the info level
	d.writers = &sync.Map{}
	d.writers.Store(constant.LoggerCombinedFileName, file)

	// Call the Info method with a message
	message := "test info message"
	if err := d.Info(message); err != nil {
		t.Fatalf("Info method returned an error: %v", err)
	}

	// Check if the message was logged to the correct file
	fileContent, err := os.ReadFile(file.Name())
	if err != nil {
		t.Fatal(err)
	}

	expectedLog := fmt.Sprintf("[test-app] <Info> %s: %s\n", strftime.Format(constant.LoggerDateTimeFormat, time.Now()), message)
	if string(fileContent) != expectedLog {
		t.Errorf("expected log to contain '%s', got %s", expectedLog, string(fileContent))
	}
}

func TestDriver_Verbose(t *testing.T) {
	// Initialize a new Driver struct for testing
	d := &Driver{
		logPath: "/var/log/myapp",
		appName: "test-app",
		scope:   "test-scope",
	}

	// Set up a mock file for testing
	file, err := os.CreateTemp("", "myapp_verbose")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	// Set up the writer for the verbose level
	d.writers = &sync.Map{}
	d.writers.Store(constant.LoggerCombinedFileName, file)

	// Call the Verbose method with a message
	message := "test verbose message"
	if err := d.Verbose(message); err != nil {
		t.Fatalf("Verbose method returned an error: %v", err)
	}

	// Check if the message was logged to the correct file
	fileContent, err := os.ReadFile(file.Name())
	if err != nil {
		t.Fatal(err)
	}

	expectedLog := fmt.Sprintf("[test-app] <Verbose> %s: %s\n", strftime.Format(constant.LoggerDateTimeFormat, time.Now()), message)
	if string(fileContent) != expectedLog {
		t.Errorf("expected log to contain '%s', got %s", expectedLog, string(fileContent))
	}
}

func TestDriver_Silly(t *testing.T) {
	// Initialize a new Driver struct for testing
	d := &Driver{
		logPath: "/var/log/myapp",
		appName: "test-app",
		scope:   "test-scope",
	}

	// Set up a mock file for testing
	file, err := os.CreateTemp("", "myapp_silly")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	// Set up the writer for the silly level
	d.writers = &sync.Map{}
	d.writers.Store(constant.LoggerCombinedFileName, file)

	// Call the Silly method with a message
	message := "test silly message"
	if err := d.Silly(message); err != nil {
		t.Fatalf("Silly method returned an error: %v", err)
	}

	// Check if the message was logged to the correct file
	fileContent, err := os.ReadFile(file.Name())
	if err != nil {
		t.Fatal(err)
	}

	expectedLog := fmt.Sprintf("[test-app] <Silly> %s: %s\n", strftime.Format(constant.LoggerDateTimeFormat, time.Now()), message)
	if string(fileContent) != expectedLog {
		t.Errorf("expected log to contain '%s', got %s", expectedLog, string(fileContent))
	}
}

func TestDriver_ApplyColorSetting(t *testing.T) {
	// Create a new color.Color instance
	c := color.New(color.FgGreen)

	// Apply color setting
	f := &Driver{}
	f.ApplyColorSetting(c)

	// Assert that color is disabled
	if c.Sprintf("") != "" {
		t.Error("Color should be disabled")
	}
}

func TestDriver_SetAppName(t *testing.T) {
	// Initialize a new Driver struct for testing
	d := &Driver{
		logPath: "/var/log/myapp",
		appName: "initial-app-name",
		scope:   "test-scope",
	}

	// Set the new app name
	d.SetAppName("new-app-name")

	// Check if the app name has been set correctly
	if d.AppName() != "new-app-name" {
		t.Errorf("expected app name to be 'new-app-name', got '%s'", d.AppName())
	}
}

func TestDriver_createWriter_withLogPath(t *testing.T) {
	// Initialize a new Driver struct for testing
	d := &Driver{
		logPath: "",
		scope:   "test-scope",
		writers: new(sync.Map),
	}

	// Set up a mock file for testing
	file, err := os.CreateTemp("", "myapp_test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	// Set the logPath to the mock file directory
	d.logPath = filepath.Dir(file.Name())

	// Call the createWriter function with a name
	name := "test"
	d.createWriter(name)

	// Check if the file was created
	_, err = os.Stat(filepath.Join(d.logPath, d.scope+"-"+name+constant.LoggerFileExtension))
	if os.IsNotExist(err) {
		t.Errorf("file %s was not created", name)
	}
}

func TestDriver_createWriter(t *testing.T) {
	// Initialize a new Driver struct for testing
	d := &Driver{
		logPath: "",
		scope:   "test-scope",
		writers: new(sync.Map),
	}

	// Call the createWriter function with a name
	name := "test"
	d.createWriter(name)

	runtimeDir, err := runtime.GetRuntimeDir()
	if err != nil {
		t.Error("failed to get the runtime directory")
	}

	// Check if the file was created
	_, err = os.Stat(filepath.Join(runtimeDir, constant.LoggerFolderName, d.scope+"-"+name+constant.LoggerFileExtension))
	if os.IsNotExist(err) {
		t.Errorf("file %s was not created", name)
	}
}

func TestDriver_getFile(t *testing.T) {
	// Initialize a new Driver struct for testing
	d := &Driver{
		logPath:        "/tmp/log/myapp",
		appName:        "test-app",
		scope:          "test-scope",
		separatedFiles: true,
		writers:        &sync.Map{},
	}

	// Call the getFile method with a name that hasn't been created yet
	name := "test-file"
	writer := d.getFile(name)

	// Check if the writer was created and stored correctly
	if writer == nil {
		t.Fatalf("getFile method returned nil for writer")
	}

	storedWriter, ok := d.writers.Load(name)
	if !ok {
		t.Fatalf("getFile method did not store the writer for the given name")
	}
	if storedWriter != writer {
		t.Fatalf("getFile method stored a different writer than the one returned")
	}

	// Call the getFile method again with the same name
	// and check if it returns the same writer instance
	secondWriter := d.getFile(name)
	if secondWriter != writer {
		t.Fatalf("getFile method did not return the same writer instance for the same name")
	}
}

func TestLoggerToken(t *testing.T) {
	expectedToken := constant.LoggerFileToken
	actualToken := LoggerToken()

	if actualToken != expectedToken {
		t.Errorf("expected token to be '%s', got '%s'", expectedToken, actualToken)
	}
}

func TestNewFile(t *testing.T) {
	scope := "test-scope"

	// Test with default options
	loggerDriver := NewFile(scope)
	driver, ok := loggerDriver.(*Driver)
	if !ok {
		t.Fatal("NewFile returned an unexpected driver type")
	}
	if driver.logPath != "" {
		t.Errorf("Expected logPath to be empty, but got %s", driver.logPath)
	}
	if driver.appName != "" {
		t.Errorf("Expected appName to be empty, but got %s", driver.appName)
	}
	if driver.scope != scope {
		t.Errorf("Expected scope to be %s, but got %s", scope, driver.scope)
	}
	if driver.separatedFiles != false {
		t.Errorf("Expected separatedFiles to be false, but got %t", driver.separatedFiles)
	}

	// Test with custom options
	logPath := "/var/log/myapp"
	appName := "test-app"
	loggerDriver = NewFile(scope, WithLogPath(logPath), WithAppName(appName), WithSeparatedFiles())
	driver, ok = loggerDriver.(*Driver)
	if !ok {
		t.Fatal("NewFile returned an unexpected driver type")
	}
	if driver.logPath != logPath {
		t.Errorf("Expected logPath to be %s, but got %s", logPath, driver.logPath)
	}
	if driver.appName != appName {
		t.Errorf("Expected appName to be %s, but got %s", appName, driver.appName)
	}
	if driver.scope != scope {
		t.Errorf("Expected scope to be %s, but got %s", scope, driver.scope)
	}
	if driver.separatedFiles != true {
		t.Errorf("Expected separatedFiles to be %t, but got %t", true, driver.separatedFiles)
	}
}
