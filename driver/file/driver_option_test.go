package file

import (
	"testing"
)

func TestFileOptions(t *testing.T) {
	// Initialize a new Driver struct for testing
	d := &Driver{}

	// Test the WithAppName function
	WithAppName("myapp")(d)
	if d.appName != "myapp" {
		t.Errorf("expected d.appName to be 'myapp', got %v", d.appName)
	}

	// Test the WithLogPath function
	WithLogPath("/var/log/myapp")(d)
	if d.logPath != "/var/log/myapp" {
		t.Errorf("expected d.logPath to be '/var/log/myapp', got %v", d.logPath)
	}

	// Test the WithSeparatedFiles function
	WithSeparatedFiles()(d)
	if d.separatedFiles != true {
		t.Errorf("expected d.separatedFiles to be true, got %v", d.separatedFiles)
	}
}

func TestWithAppNameWithoutValidDriver(t *testing.T) {
	err := WithAppName("test-app")(nil)

	if err == nil {
		t.Error("WithAppName without a valid driver must return an error")
	}
}

func TestWithLogPathWithoutValidDriver(t *testing.T) {
	err := WithLogPath("/var/log/myapp")(nil)

	if err == nil {
		t.Error("WithLogPath without a valid driver must return an error")
	}
}

func TestWithSeparatedFiles(t *testing.T) {
	err := WithSeparatedFiles()(nil)

	if err == nil {
		t.Error("WithSeparatedFiles without a valid driver must return an error")
	}
}
