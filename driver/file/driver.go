package file

import (
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"sync"

	"github.com/fatih/color"
	"gitlab.com/hestia-go/core/iface"
	"gitlab.com/hestia-go/core/is"
	"gitlab.com/hestia-go/core/runtime"
	"gitlab.com/hestia-go/logger/constant"
	"gitlab.com/hestia-go/logger/formatter"
	_interface "gitlab.com/hestia-go/logger/interface"
)

// Driver Driver logger structure
type Driver struct {
	separatedFiles bool
	scope          string
	writers        *sync.Map
	appName        string
	logPath        string
	mutex          sync.Mutex
}

// AppName Return the app name
func (f *Driver) AppName() string {
	return f.appName
}

// Debug Log message to file in debug level
func (f *Driver) Debug(message string) error {
	_, err := fmt.Fprintln(f.getFile(constant.LoggerDebugLevel), formatter.FormatStd(constant.LoggerDebugLevel, message, f))

	return err
}

// ErrorString Log error message to file in error level
func (f *Driver) ErrorString(err string) error {
	_, errFmt := fmt.Fprintln(f.getFile(constant.LoggerErrorLevel), formatter.FormatErrorString(err, f))

	return errFmt
}

// Error Log error object to file in error level
func (f *Driver) Error(err error) error {
	return f.ErrorString(formatter.FormatError(err))
}

// Exception Log exception to file in error level
func (f *Driver) Exception(err iface.Exception) error {
	return f.ErrorString(formatter.FormatException(err))
}

// Warn Log message to file in warn level
func (f *Driver) Warn(message string) error {
	_, err := fmt.Fprintln(f.getFile(constant.LoggerWarnLevel), formatter.FormatStd(constant.LoggerWarnLevel, message, f))

	return err
}

// Info Log message to file in info level
func (f *Driver) Info(message string) error {
	_, err := fmt.Fprintln(f.getFile(constant.LoggerInfoLevel), formatter.FormatStd(constant.LoggerInfoLevel, message, f))

	return err
}

// Verbose Log message to file in verbose level
func (f *Driver) Verbose(message string) error {
	_, err := fmt.Fprintln(f.getFile(constant.LoggerVerboseLevel), formatter.FormatStd(constant.LoggerVerboseLevel, message, f))

	return err
}

// Silly Log message to file in silly level
func (f *Driver) Silly(message string) error {
	_, err := fmt.Fprintln(f.getFile(constant.LoggerSillyLevel), formatter.FormatStd(constant.LoggerSillyLevel, message, f))

	return err
}

// ApplyColorSetting apply the color setting on the given color
func (*Driver) ApplyColorSetting(color *color.Color) *color.Color {
	color.DisableColor()

	return color
}

// SetAppName Set app name
func (f *Driver) SetAppName(appName string) {
	f.appName = appName
}

// createWriter Create a file write for the given name
func (f *Driver) createWriter(name string) {
	if f.logPath == "" {
		runtimeDir, err := runtime.GetRuntimeDir()

		// Skip test of this branch the error will normally never raise
		if !is.Nil(err) { // skipcq: TCV-001
			log.Println(err)
		}

		f.logPath = filepath.Join(runtimeDir, constant.LoggerFolderName)
	}

	if _, err := os.Stat(f.logPath); os.IsNotExist(err) {
		// Added skipcq, the log path must be read by another process like collectors
		err := os.MkdirAll(f.logPath, 0o775) // skipcq: GSC-G301

		// Skip test of this branch the error can be raised but not important
		if err != nil { // skipcq: TCV-001
			log.Println(err)
		}
	}

	file := filepath.Join(f.logPath, f.scope+"-"+name+constant.LoggerFileExtension)

	// Added skipcq, the logs files must be read by another process like collectors
	openFile, err := os.OpenFile(file, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0o664) // skipcq: GSC-G302

	if !is.Nil(err) {
		log.Println(err)
	}

	f.writers.Store(name, openFile)
}

// getFile Create and / or return file writer for the given name
func (f *Driver) getFile(name string) io.Writer {
	if !f.separatedFiles {
		name = constant.LoggerCombinedFileName
	}

	writer, ok := f.writers.Load(name)

	if !ok {
		f.mutex.Lock()
		defer f.mutex.Unlock()

		writer, ok = f.writers.Load(name)

		if !ok {
			f.createWriter(name)
			writer, _ = f.writers.Load(name)
		}
	}

	return writer.(io.Writer)
}

// LoggerToken return the file logger token
func LoggerToken() string {
	return constant.LoggerFileToken
}

// NewFile Create a new file logger driver
func NewFile(scope string, opts ..._interface.DriverOption) _interface.LoggerDriver {
	var file = new(Driver)

	for _, opt := range opts {
		err := opt(file)

		if err != nil {
			log.Printf("Failed to apply an option on file logger scoped: %s, error: %+v\n", scope, err)
		}
	}

	file.scope = scope

	return file
}
