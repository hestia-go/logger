package console

import "testing"

func TestWithAppName(t *testing.T) {
	d := &Driver{}
	appName := "test-app"
	WithAppName(appName)(d)
	if d.appName != appName {
		t.Errorf("WithAppName did not set the correct app name. Expected %s, got %s", appName, d.appName)
	}
}

func TestWithAppNameWithValidDriver(t *testing.T) {
	err := WithAppName("test-app")(nil)

	if err == nil {
		t.Error("WithAppName with invalid driver provided must return an error")
	}
}

func TestWithDisabledColors(t *testing.T) {
	d := &Driver{}
	WithDisabledColors()(d)
	if !d.disabledColors {
		t.Error("WithDisabledColors did not disable colors")
	}
}

func TestWithDisabledColorsWithoutValidDriver(t *testing.T) {
	err := WithDisabledColors()(nil)

	if err == nil {
		t.Error("WithDisabledColors with invalid driver provided must return an error")
	}
}
