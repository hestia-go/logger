package console

import (
	"errors"
	"fmt"
	"os"
	"strings"
	"testing"
	"time"

	"github.com/fatih/color"
	"github.com/hhkbp2/go-strftime"
	"gitlab.com/hestia-go/core/exception"
	"gitlab.com/hestia-go/logger/constant"
	_interface "gitlab.com/hestia-go/logger/interface"
)

func TestDriver_AppName(t *testing.T) {
	d := &Driver{appName: "my-app"}
	expectedAppName := "my-app"
	if d.AppName() != expectedAppName {
		t.Errorf("expected app name %s but got %s", expectedAppName, d.AppName())
	}
}

func TestDriver_Debug(t *testing.T) {
	// Create a temporary file for outputStd
	outputStd, err := os.CreateTemp("", "outputStd")
	if err != nil {
		t.Fatalf("failed to create temporary file: %v", err)
	}
	defer os.Remove(outputStd.Name())

	d := &Driver{
		disabledColors: false,
		scope:          "test-scope",
		appName:        "test-app",
		OutputStd:      outputStd,
		OutputErr:      os.Stderr,
	}

	// Call the Debug method
	if err := d.Debug("test message"); err != nil {
		t.Fatalf("Debug method returned an error: %v", err)
	}

	// Check the output in outputStd file
	outputBytes, err := os.ReadFile(outputStd.Name())
	if err != nil {
		t.Fatalf("failed to read outputStd file: %v", err)
	}
	green := d.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()
	white := d.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	blue := d.ApplyColorSetting(color.New(color.FgBlue)).SprintfFunc()
	expectedOutput := fmt.Sprintf(
		constant.LoggerStdFormat,
		green(constant.LoggerAppNameFormat, "test-app"),
		"Debug",
		white("%s", strftime.Format(constant.LoggerDateTimeFormat, time.Now())),
		"test message",
	)
	expectedOutput = blue(expectedOutput) + "\n"
	if string(outputBytes) != expectedOutput {
		t.Errorf("expected output %q but got %q", expectedOutput, string(outputBytes))
	}
}

func TestDriver_ErrorString(t *testing.T) {
	// Create a temporary file for outputErr
	outputErr, err := os.CreateTemp("", "outputErr")
	if err != nil {
		t.Fatalf("failed to create temporary file: %v", err)
	}
	defer os.Remove(outputErr.Name())

	d := &Driver{
		disabledColors: false,
		scope:          "test-scope",
		appName:        "test-app",
		OutputStd:      os.Stdout,
		OutputErr:      outputErr,
	}

	// Call the ErrorString method
	if err := d.ErrorString("test error message"); err != nil {
		t.Fatalf("ErrorString method returned an error: %v", err)
	}

	// Check the output in outputErr file
	outputBytes, err := os.ReadFile(outputErr.Name())
	if err != nil {
		t.Fatalf("failed to read outputErr file: %v", err)
	}
	green := d.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()
	white := d.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	red := d.ApplyColorSetting(color.New(color.FgRed)).SprintfFunc()
	expectedOutput := fmt.Sprintf(
		constant.LoggerErrorFormat,
		green(constant.LoggerAppNameFormat, "test-app"),
		white("%s", strftime.Format(constant.LoggerDateTimeFormat, time.Now())),
		"test error message",
	)
	expectedOutput = red(expectedOutput) + "\n"
	if string(outputBytes) != expectedOutput {
		t.Errorf("expected output %q but got %q", expectedOutput, string(outputBytes))
	}
}

func TestDriver_Error(t *testing.T) {
	// Create a temporary file for outputErr
	outputErr, err := os.CreateTemp("", "outputErr")
	if err != nil {
		t.Fatalf("failed to create temporary file: %v", err)
	}
	defer os.Remove(outputErr.Name())

	d := &Driver{
		disabledColors: false,
		scope:          "test-scope",
		appName:        "test-app",
		OutputStd:      os.Stdout,
		OutputErr:      outputErr,
	}

	// Call the Error method with a test error
	localError := errors.New("test error")
	if err := d.Error(localError); err != nil {
		t.Fatalf("Error method returned an error: %v", err)
	}

	// Check the output in outputErr file
	outputBytes, err := os.ReadFile(outputErr.Name())
	if err != nil {
		t.Fatalf("failed to read outputErr file: %v", err)
	}

	green := d.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()
	white := d.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	red := d.ApplyColorSetting(color.New(color.FgRed)).SprintfFunc()
	expectedOutput := fmt.Sprintf(
		constant.LoggerErrorFormat,
		green(constant.LoggerAppNameFormat, "test-app"),
		white("%s", strftime.Format(constant.LoggerDateTimeFormat, time.Now())),
		localError.Error(),
	)
	expectedOutput = red(expectedOutput)
	expectedOutput = expectedOutput[:71]
	if !strings.Contains(string(outputBytes), expectedOutput) {
		t.Errorf("expected string start %q but got %q", expectedOutput, string(outputBytes))
	}
}

func TestDriver_Exception(t *testing.T) {
	// Create a temporary file for outputErr
	outputErr, err := os.CreateTemp("", "outputErr")
	if err != nil {
		t.Fatalf("failed to create temporary file: %v", err)
	}
	defer os.Remove(outputErr.Name())

	d := &Driver{
		disabledColors: false,
		scope:          "test-scope",
		appName:        "test-app",
		OutputStd:      os.Stdout,
		OutputErr:      outputErr,
	}

	// Call the Exception method with a test exception
	localException := exception.NewException(exception.WithCode("test-code"), exception.WithMessage("test exception message"))
	if err := d.Exception(localException); err != nil {
		t.Fatalf("Exception method returned an error: %v", err)
	}

	// Check the output in outputErr file
	outputBytes, err := os.ReadFile(outputErr.Name())
	if err != nil {
		t.Fatalf("failed to read outputErr file: %v", err)
	}
	// expectedOutput := fmt.Sprintf("%s %s\n", color.RedString("[test-scope]"), "[test-code] test exception message")
	expectedOutput := fmt.Sprintf(
		constant.LoggerExceptionFormat,
		localException.Code(),
		"",
		localException.Message(),
		"",
	)

	green := d.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()
	white := d.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	red := d.ApplyColorSetting(color.New(color.FgRed)).SprintfFunc()
	expectedOutput = fmt.Sprintf(
		constant.LoggerErrorFormat,
		green(constant.LoggerAppNameFormat, "test-app"),
		white("%s", strftime.Format(constant.LoggerDateTimeFormat, time.Now())),
		expectedOutput,
	)
	expectedOutput = red(expectedOutput)
	expectedOutput = expectedOutput[:109]
	if !strings.Contains(string(outputBytes), expectedOutput) {
		t.Errorf("expected output %q but got %q", expectedOutput, string(outputBytes))
	}
}

func TestDriver_Warn(t *testing.T) {
	// Create a temporary file for outputStd
	outputStd, err := os.CreateTemp("", "outputStd")
	if err != nil {
		t.Fatalf("failed to create temporary file: %v", err)
	}
	defer os.Remove(outputStd.Name())

	d := &Driver{
		disabledColors: false,
		scope:          "test-scope",
		appName:        "test-app",
		OutputStd:      outputStd,
		OutputErr:      os.Stderr,
	}

	// Call the Warn method with a test message
	if err := d.Warn("test warn message"); err != nil {
		t.Fatalf("Warn method returned an error: %v", err)
	}

	// Check the output in outputStd file
	outputBytes, err := os.ReadFile(outputStd.Name())
	if err != nil {
		t.Fatalf("failed to read outputStd file: %v", err)
	}
	green := d.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()
	white := d.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	yellow := d.ApplyColorSetting(color.New(color.FgYellow)).SprintfFunc()
	expectedOutput := fmt.Sprintf(
		constant.LoggerStdFormat,
		green(constant.LoggerAppNameFormat, "test-app"),
		"Warn",
		white("%s", strftime.Format(constant.LoggerDateTimeFormat, time.Now())),
		"test warn message",
	)
	expectedOutput = yellow(expectedOutput) + "\n"
	if string(outputBytes) != expectedOutput {
		t.Errorf("expected output %q but got %q", expectedOutput, string(outputBytes))
	}
}

func TestDriver_Info(t *testing.T) {
	// Create a temporary file for outputStd
	outputStd, err := os.CreateTemp("", "outputStd")
	if err != nil {
		t.Fatalf("failed to create temporary file: %v", err)
	}
	defer os.Remove(outputStd.Name())

	d := &Driver{
		disabledColors: false,
		scope:          "test-scope",
		appName:        "test-app",
		OutputStd:      outputStd,
		OutputErr:      os.Stderr,
	}

	// Call the Info method with a test message
	if err := d.Info("test info message"); err != nil {
		t.Fatalf("Info method returned an error: %v", err)
	}

	// Check the output in outputStd file
	outputBytes, err := os.ReadFile(outputStd.Name())
	if err != nil {
		t.Fatalf("failed to read outputStd file: %v", err)
	}
	green := d.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()
	white := d.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	cyan := d.ApplyColorSetting(color.New(color.FgCyan)).SprintfFunc()
	expectedOutput := fmt.Sprintf(
		constant.LoggerStdFormat,
		green(constant.LoggerAppNameFormat, "test-app"),
		"Info",
		white("%s", strftime.Format(constant.LoggerDateTimeFormat, time.Now())),
		"test info message",
	)
	expectedOutput = cyan(expectedOutput) + "\n"
	if string(outputBytes) != expectedOutput {
		t.Errorf("expected output %q but got %q", expectedOutput, string(outputBytes))
	}
}

func TestDriver_Verbose(t *testing.T) {
	// Create a temporary file for outputStd
	outputStd, err := os.CreateTemp("", "outputStd")
	if err != nil {
		t.Fatalf("failed to create temporary file: %v", err)
	}
	defer os.Remove(outputStd.Name())

	d := &Driver{
		disabledColors: false,
		scope:          "test-scope",
		appName:        "test-app",
		OutputStd:      outputStd,
		OutputErr:      os.Stderr,
	}

	// Call the Verbose method with a test message
	if err := d.Verbose("test verbose message"); err != nil {
		t.Fatalf("Verbose method returned an error: %v", err)
	}

	// Check the output in outputStd file
	outputBytes, err := os.ReadFile(outputStd.Name())
	if err != nil {
		t.Fatalf("failed to read outputStd file: %v", err)
	}
	green := d.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()
	white := d.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	hiCyan := d.ApplyColorSetting(color.New(color.FgHiCyan)).SprintfFunc()
	expectedOutput := fmt.Sprintf(
		constant.LoggerStdFormat,
		green(constant.LoggerAppNameFormat, "test-app"),
		"Verbose",
		white("%s", strftime.Format(constant.LoggerDateTimeFormat, time.Now())),
		"test verbose message",
	)
	expectedOutput = hiCyan(expectedOutput) + "\n"
	if string(outputBytes) != expectedOutput {
		t.Errorf("expected output %q but got %q", expectedOutput, string(outputBytes))
	}
}

func TestDriver_Silly(t *testing.T) {
	// Create a temporary file for outputStd
	outputStd, err := os.CreateTemp("", "outputStd")
	if err != nil {
		t.Fatalf("failed to create temporary file: %v", err)
	}
	defer os.Remove(outputStd.Name())

	d := &Driver{
		disabledColors: false,
		scope:          "test-scope",
		appName:        "test-app",
		OutputStd:      outputStd,
		OutputErr:      os.Stderr,
	}

	// Call the Silly method with a test message
	if err := d.Silly("test silly message"); err != nil {
		t.Fatalf("Silly method returned an error: %v", err)
	}

	// Check the output in outputStd file
	outputBytes, err := os.ReadFile(outputStd.Name())
	if err != nil {
		t.Fatalf("failed to read outputStd file: %v", err)
	}
	green := d.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()
	white := d.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	hiWhite := d.ApplyColorSetting(color.New(color.FgHiWhite)).SprintfFunc()
	expectedOutput := fmt.Sprintf(
		constant.LoggerStdFormat,
		green(constant.LoggerAppNameFormat, "test-app"),
		"Silly",
		white("%s", strftime.Format(constant.LoggerDateTimeFormat, time.Now())),
		"test silly message",
	)
	expectedOutput = hiWhite(expectedOutput) + "\n"
	if string(outputBytes) != expectedOutput {
		t.Errorf("expected output %q but got %q", expectedOutput, string(outputBytes))
	}
}

func TestDriver_SetAppName(t *testing.T) {
	d := &Driver{}
	d.SetAppName("test app")
	if d.appName != "test app" {
		t.Errorf("AppName not set correctly")
	}
}

func TestDriver_ApplyColorSetting(t *testing.T) {
	d := &Driver{disabledColors: true}
	c := color.New(color.FgRed)
	d.ApplyColorSetting(c)
	v := c.SprintFunc()("")
	if v != "" {
		t.Errorf("Color setting not applied correctly")
	}
}

func TestLoggerToken(t *testing.T) {
	token := LoggerToken()
	if token != constant.LoggerConsoleToken {
		t.Errorf("LoggerToken not returned correctly")
	}
}

func TestNewConsole(t *testing.T) {
	console := NewConsole("test scope")

	if c, ok := console.(*Driver); ok {
		if c.scope != "test scope" {
			t.Errorf("Scope not set correctly")
		}
		if c.OutputErr != os.Stderr {
			t.Errorf("OutputErr not set correctly")
		}
		if c.OutputStd != os.Stdout {
			t.Errorf("OutputStd not set correctly")
		}
	} else {
		t.Errorf("NewConsole not returning an instance of driver")
	}
}

func TestNewConsoleWithOption(t *testing.T) {
	console := NewConsole("test", WithDisabledColors())

	if c, ok := console.(*Driver); ok {
		if c.scope != "test" {
			t.Error("Scope not set")
		}
		if c.disabledColors == false {
			t.Error("Disabled color option not applied")
		}
	} else {
		t.Error("NewConsole does not return an instance of the console driver")
	}
}

func TestNewConsoleWithOptionReturningError(t * testing.T) {
	opt := func (d _interface.LoggerDriver) error {
		return errors.New("Failing opt")
	}

	console := NewConsole("test", opt)

	if _, ok := console.(*Driver); !ok {
		t.Error("Errored option must not interrupt logger instantiation")
	}
}
