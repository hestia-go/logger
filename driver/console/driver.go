package console

import (
	"fmt"
	"log"
	"os"

	"github.com/fatih/color"
	"gitlab.com/hestia-go/core/iface"
	"gitlab.com/hestia-go/logger/constant"
	"gitlab.com/hestia-go/logger/formatter"
	_interface "gitlab.com/hestia-go/logger/interface"
)

// Driver structure
type Driver struct {
	disabledColors bool
	scope          string
	appName        string
	OutputStd      *os.File
	OutputErr      *os.File
}

// AppName Return the app name
func (c *Driver) AppName() string {
	return c.appName
}

// Debug Log message in debug level
func (c *Driver) Debug(message string) error {
	blue := c.ApplyColorSetting(color.New(color.FgBlue)).SprintfFunc()

	_, err := fmt.Fprintln(c.OutputStd, blue(formatter.FormatStd(constant.LoggerDebugLevel, message, c)))

	return err
}

// ErrorString Log error message in error level
func (c *Driver) ErrorString(err string) error {
	red := c.ApplyColorSetting(color.New(color.FgRed)).SprintFunc()

	_, errFmt := fmt.Fprintln(c.OutputErr, red(formatter.FormatErrorString(err, c)))

	return errFmt
}

// Error Log error object in error level
func (c *Driver) Error(err error) error {
	return c.ErrorString(formatter.FormatError(err))
}

// Exception Log exception in error level
func (c *Driver) Exception(err iface.Exception) error {
	return c.ErrorString(formatter.FormatException(err))
}

// Warn Log message in warn level
func (c *Driver) Warn(message string) error {
	yellow := c.ApplyColorSetting(color.New(color.FgYellow)).SprintFunc()

	_, err := fmt.Fprintln(c.OutputStd, yellow(formatter.FormatStd(constant.LoggerWarnLevel, message, c)))

	return err
}

// Info Log message in info level
func (c *Driver) Info(message string) error {
	cyan := c.ApplyColorSetting(color.New(color.FgCyan)).SprintFunc()

	_, err := fmt.Fprintln(c.OutputStd, cyan(formatter.FormatStd(constant.LoggerInfoLevel, message, c)))

	return err
}

// Verbose Log message in verbose level
func (c *Driver) Verbose(message string) error {
	hiCyan := c.ApplyColorSetting(color.New(color.FgHiCyan)).SprintFunc()

	_, err := fmt.Fprintln(c.OutputStd, hiCyan(formatter.FormatStd(constant.LoggerVerboseLevel, message, c)))

	return err
}

// Silly Log message in silly level
func (c *Driver) Silly(message string) error {
	hiWhite := c.ApplyColorSetting(color.New(color.FgHiWhite)).SprintFunc()

	_, err := fmt.Fprintln(c.OutputStd, hiWhite(formatter.FormatStd(constant.LoggerSillyLevel, message, c)))

	return err
}

// SetAppName Set the app name
func (c *Driver) SetAppName(appName string) {
	c.appName = appName
}

// ApplyColorSetting Apply the color setting for the color package
func (c *Driver) ApplyColorSetting(color *color.Color) *color.Color {
	if c.disabledColors {
		color.DisableColor()
		return color
	}

	color.EnableColor()
	return color
}

// LoggerToken Driver logger token
func LoggerToken() string {
	return constant.LoggerConsoleToken
}

// NewConsole Create a new instance of console logger
func NewConsole(scope string, opts ..._interface.DriverOption) _interface.LoggerDriver {
	var console = new(Driver)

	for _, opt := range opts {
		err := opt(console)

		if err != nil {
			log.Printf("Failed to apply an option on console logger scoped: %s, error: %+v\n", scope, err)
		}
	}

	console.scope = scope
	console.OutputErr = os.Stderr
	console.OutputStd = os.Stdout

	return console
}
