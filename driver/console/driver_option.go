package console

import (
	"gitlab.com/hestia-go/core/exception"
	_interface "gitlab.com/hestia-go/logger/interface"
)

// WithAppName set the application name in the console driver
func WithAppName(appName string) _interface.DriverOption {
	return func(d _interface.LoggerDriver) error {
		if driver, ok := d.(*Driver); ok {
			driver.appName = appName
			return nil
		}

		return exception.NewException(exception.WithMessage("The driver must be an instance of Console driver for option WithAppName"))
	}
}

// WithDisabledColors disable the output colors
func WithDisabledColors() _interface.DriverOption {
	return func(d _interface.LoggerDriver) error {
		if driver, ok := d.(*Driver); ok {
			driver.disabledColors = true
			return nil
		}

		return exception.NewException(exception.WithMessage("The driver must be an instance of Console driver for option WithDisabledColors"))
	}
}
