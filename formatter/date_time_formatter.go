package formatter

import (
	"time"

	"github.com/fatih/color"
	"github.com/hhkbp2/go-strftime"
	"gitlab.com/hestia-go/logger/constant"
	_interface "gitlab.com/hestia-go/logger/interface"
)

// GetDateTime Return formatted datetime
func GetDateTime(driver _interface.LoggerDriver) string {
	white := driver.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	return white("%s", strftime.Format(constant.LoggerDateTimeFormat, time.Now()))
}
