package formatter

import (
	"fmt"
	"gitlab.com/hestia-go/logger/constant"
	_interface "gitlab.com/hestia-go/logger/interface"
)

// FormatErrorString Return formatted error string
func FormatErrorString(err string, driver _interface.LoggerDriver) string {
	return fmt.Sprintf(constant.LoggerErrorFormat, GetAppName(driver), GetDateTime(driver), err)
}
