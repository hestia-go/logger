package formatter

import (
	"fmt"
	"testing"
	"time"

	"github.com/fatih/color"
	"github.com/hhkbp2/go-strftime"
	"gitlab.com/hestia-go/core/iface"
	"gitlab.com/hestia-go/logger/constant"
)

type driverTestFormatStd struct {
}

func (d *driverTestFormatStd) ErrorString(_ string) error {
	return nil
}

func (d *driverTestFormatStd) Error(_ error) error {
	return nil
}

func (d *driverTestFormatStd) Exception(_ iface.Exception) error {
	return nil
}

func (d *driverTestFormatStd) Warn(_ string) error {
	return nil
}

func (d *driverTestFormatStd) Info(_ string) error {
	return nil
}

func (d *driverTestFormatStd) Verbose(_ string) error {
	return nil
}

func (d *driverTestFormatStd) Debug(_ string) error {
	return nil
}

func (d *driverTestFormatStd) Silly(_ string) error {
	return nil
}

func (d *driverTestFormatStd) AppName() string {
	return "app-name"
}

func (d *driverTestFormatStd) SetAppName(_ string) {

}

func (d *driverTestFormatStd) ApplyColorSetting(color *color.Color) *color.Color {
	color.DisableColor()
	return color
}

func TestFormatStd(t *testing.T) {
	formatted := FormatStd("Info", "test message", &driverTestFormatStd{})

	expected := fmt.Sprintf(
		constant.LoggerStdFormat,
		fmt.Sprintf(constant.LoggerAppNameFormat, "app-name"),
		"Info",
		strftime.Format(constant.LoggerDateTimeFormat, time.Now()),
		"test message",
	)

	if formatted != expected {
		t.Errorf("expected %s got %s", expected, formatted)
	}
}
