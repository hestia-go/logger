package formatter

import (
	"fmt"
	"github.com/pkg/errors"
)

// FormatError Return formatted error
func FormatError(err error) string {
	return fmt.Sprintf("%+v", errors.WithStack(err))
}
