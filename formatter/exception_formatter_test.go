package formatter

import (
	"strings"
	"testing"

	"gitlab.com/hestia-go/core/exception"
)

func TestFormatException(t *testing.T) {
	err := exception.NewException(exception.WithCode("CODE001"), exception.WithMessage("Example exception message"), exception.WithFatal())
	expected := "[CODE001-FATAL] Example exception message\n"
	actual := FormatException(err)
	if !strings.Contains(actual, expected) {
		t.Errorf("Expected %s, but got %s", expected, actual)
	}
}
