package formatter

import (
	"testing"
	"time"

	"github.com/fatih/color"
	"github.com/hhkbp2/go-strftime"
	"gitlab.com/hestia-go/logger/constant"
)

func TestGetDateTime(t *testing.T) {
	driver := &StubDriver{}

	white := driver.ApplyColorSetting(color.New(color.FgWhite)).SprintfFunc()
	expected := white(strftime.Format(constant.LoggerDateTimeFormat, time.Now()))
	actual := GetDateTime(driver)

	if actual != expected {
		t.Errorf("Expected %s, but got %s", expected, actual)
	}
}
