package formatter

import (
	"testing"

	"github.com/fatih/color"
	"gitlab.com/hestia-go/core/iface"
)

type StubDriver struct {
	appName string
}

func (s *StubDriver) ErrorString(err string) error {
	return nil
}

func (s *StubDriver) Error(err error) error {
	return nil
}

func (s *StubDriver) Exception(exception iface.Exception) error {
	return nil
}

func (s *StubDriver) Warn(message string) error {
	return nil
}

func (s *StubDriver) Info(message string) error {
	return nil
}

func (s *StubDriver) Verbose(message string) error {
	return nil
}

func (s *StubDriver) Debug(message string) error {
	return nil
}

func (s *StubDriver) Silly(message string) error {
	return nil
}

func (s *StubDriver) AppName() string {
	return s.appName
}

func (s *StubDriver) SetAppName(appName string) {
	s.appName = appName
}

func (s *StubDriver) ApplyColorSetting(color *color.Color) *color.Color {
	color.EnableColor()
	return color
}

func TestGetAppName(t *testing.T) {
	driver := &StubDriver{
		appName: "my-app",
	}

	green := driver.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()
	expected := green("[%s]", driver.AppName())
	actual := GetAppName(driver)

	if actual != expected {
		t.Errorf("Expected %s, but got %s", expected, actual)
	}
}
