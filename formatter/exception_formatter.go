package formatter

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/hestia-go/core/iface"
	"gitlab.com/hestia-go/logger/constant"
)

// FormatException Return formatted exception
func FormatException(err iface.Exception) string {
	fatalSuffix := ""
	if err.Fatal() {
		fatalSuffix = constant.LoggerExceptionFatalSuffix
	}
	return fmt.Sprintf(constant.LoggerExceptionFormat, err.Code(), fatalSuffix, err.Message(), errors.WithStack(err.Unwrap()))
}
