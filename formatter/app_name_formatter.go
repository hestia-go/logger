package formatter

import (
	"github.com/fatih/color"
	"gitlab.com/hestia-go/logger/constant"
	_interface "gitlab.com/hestia-go/logger/interface"
)

// GetAppName Return formatted driver app name
func GetAppName(driver _interface.LoggerDriver) string {
	green := driver.ApplyColorSetting(color.New(color.FgGreen)).SprintfFunc()

	return green(constant.LoggerAppNameFormat, driver.AppName())
}
