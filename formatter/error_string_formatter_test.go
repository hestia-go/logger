package formatter

import (
	"fmt"
	"testing"
)

func TestFormatErrorString(t *testing.T) {
	driver := &StubDriver{
		appName: "my-app",
	}

	errStr := "Something went wrong"
	expected := fmt.Sprintf("%s %s Error: %s", GetAppName(driver), GetDateTime(driver), errStr)
	actual := FormatErrorString(errStr, driver)

	if actual != expected {
		t.Errorf("Expected %s, but got %s", expected, actual)
	}
}
