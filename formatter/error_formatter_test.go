package formatter

import (
	"errors"
	"strings"
	"testing"
)

func TestFormatError(t *testing.T) {
	err := errors.New("test error")
	formatted := FormatError(err)

	if !strings.Contains(formatted, "test error") {
		t.Errorf("Expected error message to be included in formatted error, but it was not: %s", formatted)
	}
}
