package formatter

import (
	"fmt"
	"gitlab.com/hestia-go/logger/constant"
	_interface "gitlab.com/hestia-go/logger/interface"
)

// FormatStd Return formatted simple message
func FormatStd(level string, message string, driver _interface.LoggerDriver) string {
	return fmt.Sprintf(constant.LoggerStdFormat, GetAppName(driver), level, GetDateTime(driver), message)
}
