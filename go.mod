module gitlab.com/hestia-go/logger

go 1.18

require (
	github.com/fatih/color v1.15.0
	github.com/hhkbp2/go-strftime v0.0.0-20150709091403-d82166ec6782
	github.com/pkg/errors v0.9.1
	gitlab.com/hestia-go/core v0.0.0-20230806145125-c2a42c6bd49f
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	golang.org/x/sys v0.9.0 // indirect
)
