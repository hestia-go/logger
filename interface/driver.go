package _interface

import (
	"github.com/fatih/color"

	"gitlab.com/hestia-go/core/iface"
)

// LoggerDriver Interface to implement for logging driver
type LoggerDriver interface {
	// ErrorString Log error string in error level
	ErrorString(err string) error
	// Error Log an error in error level
	Error(err error) error
	// Exception Log an exception in error level
	Exception(exception iface.Exception) error
	// Warn Log message in warning channel
	Warn(message string) error
	// Info Log message in info channel
	Info(message string) error
	// Verbose Log message in verbose level
	Verbose(message string) error
	// Debug Log message in debug level
	Debug(message string) error
	// Silly Log message in silly level
	Silly(message string) error
	// AppName Return the app name
	AppName() string
	// SetAppName Set the app name for logger
	SetAppName(appName string)
	// ApplyColorSetting Set the color state of output
	ApplyColorSetting(color *color.Color) *color.Color
}
