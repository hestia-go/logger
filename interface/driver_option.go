package _interface

// DriverOption define the default type for driver options
type DriverOption func(LoggerDriver) error
