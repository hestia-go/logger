package constant

import (
	"testing"
)

func TestConstants(t *testing.T) {
	expectedValues := map[string]string{
		"LoggerLogScope":             "logger",
		"LoggerDefaultLoggerScope":   "Default",
		"LoggerRegistryPrefix":       "logger-",
		"LoggerDefaultAppName":       "App",
		"LoggerDefaultAllowedScopes": "*",
		"LoggerConsoleToken":         "console",
		"LoggerFileToken":            "file",
		"LoggerDebugLevel":           "Debug",
		"LoggerInfoLevel":            "Info",
		"LoggerSillyLevel":           "Silly",
		"LoggerVerboseLevel":         "Verbose",
		"LoggerWarnLevel":            "Warn",
		"LoggerErrorLevel":           "Error",
		"LoggerFolderName":           "logs",
		"LoggerFileExtension":        ".log",
		"LoggerCombinedFileName":     "combined",
		"LoggerAppNameFormat":        "[%s]",
		"LoggerDateTimeFormat":       "%Y-%m-%d %H:%M:%S",
		"LoggerErrorFormat":          "%s %s Error: %s",
		"LoggerExceptionFatalSuffix": "-FATAL",
		"LoggerExceptionFormat":      "[%s%s] %s\nInner Error: %+v",
		"LoggerStdFormat":            "%s <%s> %s: %s",
	}

	for name, expected := range expectedValues {
		if value := eval(name); value != expected {
			t.Errorf("Constant %s has unexpected value: expected %s, got %s", name, expected, value)
		}
	}
}

func eval(name string) string {
	switch name {
	case "LoggerLogScope":
		return LoggerLogScope
	case "LoggerDefaultLoggerScope":
		return LoggerDefaultLoggerScope
	case "LoggerRegistryPrefix":
		return LoggerRegistryPrefix
	case "LoggerDefaultAppName":
		return LoggerDefaultAppName
	case "LoggerDefaultAllowedScopes":
		return LoggerDefaultAllowedScopes
	case "LoggerConsoleToken":
		return LoggerConsoleToken
	case "LoggerFileToken":
		return LoggerFileToken
	case "LoggerDebugLevel":
		return LoggerDebugLevel
	case "LoggerInfoLevel":
		return LoggerInfoLevel
	case "LoggerSillyLevel":
		return LoggerSillyLevel
	case "LoggerVerboseLevel":
		return LoggerVerboseLevel
	case "LoggerWarnLevel":
		return LoggerWarnLevel
	case "LoggerErrorLevel":
		return LoggerErrorLevel
	case "LoggerFolderName":
		return LoggerFolderName
	case "LoggerFileExtension":
		return LoggerFileExtension
	case "LoggerCombinedFileName":
		return LoggerCombinedFileName
	case "LoggerAppNameFormat":
		return LoggerAppNameFormat
	case "LoggerDateTimeFormat":
		return LoggerDateTimeFormat
	case "LoggerErrorFormat":
		return LoggerErrorFormat
	case "LoggerExceptionFatalSuffix":
		return LoggerExceptionFatalSuffix
	case "LoggerExceptionFormat":
		return LoggerExceptionFormat
	case "LoggerStdFormat":
		return LoggerStdFormat
	default:
		return ""
	}
}
