package constant

const (
	// LoggerLogScope Scope of the internal logger logs
	LoggerLogScope = "logger"
	// LoggerDefaultLoggerScope Default logger scope
	LoggerDefaultLoggerScope = "Default"
	// LoggerRegistryPrefix Prefix of loggers in the registry
	LoggerRegistryPrefix = "logger-"
	// LoggerDefaultAppName Logger default app name
	LoggerDefaultAppName = "App"
	// LoggerDefaultAllowedScopes Default allowed log scopes
	LoggerDefaultAllowedScopes = "*"
	// LoggerConsoleToken Token of the console logger
	LoggerConsoleToken = "console"
	// LoggerFileToken Token of the file logger
	LoggerFileToken = "file"
	// LoggerDebugLevel Debug logger level identifier
	LoggerDebugLevel = "Debug"
	// LoggerInfoLevel Info logger level identifier
	LoggerInfoLevel = "Info"
	// LoggerSillyLevel Silly logger level identifier
	LoggerSillyLevel = "Silly"
	// LoggerVerboseLevel Verbose logger level identifier
	LoggerVerboseLevel = "Verbose"
	// LoggerWarnLevel Warning logger level identifier
	LoggerWarnLevel = "Warn"
	// LoggerErrorLevel Error logger level identifier
	LoggerErrorLevel = "Error"
	// LoggerFolderName Default folder name of the file logger
	LoggerFolderName = "logs"
	// LoggerFileExtension Default extension of the file logs
	LoggerFileExtension = ".log"
	// LoggerCombinedFileName Default name of the combined log file
	LoggerCombinedFileName = "combined"
	// LoggerAppNameFormat Default format of the app name in logs
	LoggerAppNameFormat = "[%s]"
	// LoggerDateTimeFormat Default format of the datetime in logs
	LoggerDateTimeFormat = "%Y-%m-%d %H:%M:%S"
	// LoggerErrorFormat Default format of the errors in logs
	LoggerErrorFormat = "%s %s Error: %s"
	// LoggerExceptionFatalSuffix Default suffix for fatal exceptions in logs
	LoggerExceptionFatalSuffix = "-FATAL"
	// LoggerExceptionFormat Default format of the exception in logs
	LoggerExceptionFormat = "[%s%s] %s\nInner Error: %+v"
	// LoggerStdFormat Default format of the logs
	LoggerStdFormat = "%s <%s> %s: %s"
)
